In order to maintain compatability some of the existing dnd5e skills were
repurposed as new genefunk skills. This file contains a mapping of from
the abbreviation used in macros and such to access the skill and ability
which it relates to both in the original dnd5e and in the obsidiangenefunk
module.


ABR      Old Name         Old Ability   New Name           New Ability
------------------------------------------------------------------------
acr      Acrobatics       dex           Acrobatics         dex
ani      Animal Handling  wis           Bureaucracy*       int*
arc      Arcana           int           Computers*         int
------------------------------------------------------------------------
ath      Athletics        str           Athletics          str
dec      Deception        cha           Deception          cha
his      History          int           Drive*             dex*
------------------------------------------------------------------------
ins      Insight          wis           Insight            wis
itm      Intimidation     cha           Intimidation       cha
inv      Investigation    int           Investigation      int
------------------------------------------------------------------------
med      Medicine         wis           Life Science*      int*
nat      Nature           int           Mechanics*         int
prc      Perception       wis           Perception         wis
------------------------------------------------------------------------
prf      Performance      cha           Performance        cha
per      Persuasion       cha           Persuasion         cha   
rel      Religion         int           Physical Science*  int
------------------------------------------------------------------------
slt      Sleight of Hand  dex           Sleight of Hand    dex
ste      Stealth          dex           Stealth            dex
sur      Survival         wis           Survial            wis
------------------------------------------------------------------------
soc      N/A              N/A           Social Science     int
str      N/A              N/A           Streetwise         int
