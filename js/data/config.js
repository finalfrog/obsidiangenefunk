// The Config object categorises and enumerates the various options available
// when building characters, items, and effects in the system. Most of these
// are informed by the game rules itself, but some are specific to obsidian's
// implementation of them.

export const Config = {};

Config.ABILITIES = ['str', 'dex', 'con', 'int', 'wis', 'cha'];
Config.WEAPON_TAGS = [
	'ap', 'auto', 'ammunition', 'close', 'finesse', 'hardknuckle', 'light',
	'mount', 'offhand', 'quick', 'rapidfire', 'reach', 'shotgun', 'silenced', 'special',
	'thrown', 'twohanded', 'versatile', 'baton', 'cryo', 'explosive', 'heap',
	'incendiary', 'shock'
];

Config.ALIGNMENTS = ['lg', 'ln', 'le', 'ng', 'n', 'ne', 'cg', 'cn', 'ce'];
Config.ALIGNMENT_PARTS_1 = ['l', 'n', 'c'];
Config.ALIGNMENT_PARTS_2 = ['g', 'n', 'e', 'u'];
Config.ATTACK_TYPES = ['melee', 'ranged'];
Config.ARMOUR_TYPES = ['light', 'medium', 'heavy', 'shield'];
Config.CONSUMABLE_TYPES = ['ammo', 'explosive', 'drug', 'poison', 'gear'];
Config.CLASS_MAP = {
	biohacker: 'bio', codehacker: 'cod', crook: 'cro', engineer: 'eng', gunfighter: 'gun', hardcase: 'har',
	samurai: 'sam', suit: 'sui'
};
Config.CLASSES = [
	'biohacker', 'codehacker', 'crook', 'engineer', 'gunfighter', 'hardcase', 'samurai', 'suit'
];

Config.CURRENCY = ['gp', 'sp', 'cp'];
Config.DAMAGE_DICE = [4, 6, 8, 10, 12];
Config.DAMAGE_TYPES = [
	'blg', 'prc', 'slh', 'apb', 'arp', 'aps', 'acd', 'cld', 'fir', 'frc', 'lig', 'ncr', 'psn', 'psy', 'rad', 'thn'
];

Config.CONDITION_LEVELS = ['imm', 'adv', 'dis'];
Config.CONDITIONS = [
	'blinded', 'charmed', 'deafened', 'frightened', 'grappled', 'incapacitated', 'invisible',
	'paralysed', 'petrified', 'poisoned', 'prone', 'restrained', 'stunned', 'unconscious'
];

Config.CONVERT_CONDITIONS = Config.CONDITIONS.concat('exhaustion');
Config.CONVERT_DAMAGE_TYPES = Config.DAMAGE_TYPES.concat('nonmagical');

Config.CLASS_HIT_DICE = {
	biohacker: 8,
	codehacker: 8,
	crook: 8,
	engineer: 8,
	gunfighter: 10,
	hardcase: 12,
	samurai: 10,
	suit: 8
};

Config.CLASS_SPELL_MODS = {
	biohacker: 'int',
	codehacker: 'int',
	engineer: 'int'
};

Config.CLASS_SPELL_PROGRESSION = {
	biohacker: 'full',
	codehacker: 'full',
	engineer: 'full'
};

Config.CLASS_SPELL_PREP = {
	biohacker: 'known',
	codehacker: 'known',
	engineer: 'known'
};

Config.CLASS_RITUALS = {
	biohacker: 'prep',
	codehacker: 'prep',
	engineer: 'prep'
};

Config.CREATURE_TAGS = [
	'any', 'psychomime', 'transgenic'
];

Config.CREATURE_TYPES = [
	'animal', 'animalhuman', 'anthroid', 'droid', 'drone', 'human', 'ai', 'slimemold', 'object'
];

Config.DEFENSE_LEVELS = ['res', 'imm', 'vuln'];
Config.EFFECT_ABILITIES = Config.ABILITIES.concat(['spell', 'finesse']);
Config.EFFECT_ADD_SPELLS_METHOD = ['innate', 'known', 'prep', 'list', 'item'];
Config.EFFECT_ADD_SPELLS_SOURCE = ['list', 'individual'];
Config.EFFECT_APPLIED_ON = ['target', 'hit', 'save'];
Config.EFFECT_ATTACK_CATS = ['weapon', 'unarmed', 'spell'];
Config.EFFECT_BONUS_LEVEL = ['chr', 'cls'];
Config.EFFECT_BONUS_METHOD = ['dice', 'formula'];
Config.EFFECT_BONUS_VALUES = ['abl', 'prof', 'chr', 'cls'];
Config.EFFECT_CONSUME_CALC = ['fixed', 'var'];
Config.EFFECT_CONSUME_SLOTS = ['any', 'class'];
Config.EFFECT_DAMAGE_TYPES = [
	'blg', 'prc', 'slh', 'apb', 'arp', 'aps', 'acd', 'cld', 'fir', 'frc', 'lig', 'ncr', 'psn', 'psy', 'rad', 'thn', 'hlg'
];

Config.EFFECT_DEFENSES = ['damage', 'condition'];
Config.EFFECT_FILTERS = ['roll', 'score'];
Config.EFFECT_FILTER_ATTACKS = ['mw', 'rw', 'mu', 'ms', 'rs'];
Config.EFFECT_FILTER_CHECKS = ['ability', 'skill', 'tool', 'init'];
Config.EFFECT_FILTER_DAMAGE = ['damage', 'attack'];
Config.EFFECT_FILTER_IS_MULTI = {
	score: {ability: 1, passive: 1, speed: 1, dc: 1},
	roll: {
		attack: 1, save: 1, damage: 1,
		check: {ability: 1, skill: 1, tool: 1}
	}
};

Config.EFFECT_FILTER_MULTI = ['any', 'some'];
Config.EFFECT_FILTER_ROLLS = ['attack', 'check', 'save', 'damage', 'hd'];
Config.EFFECT_FILTER_SAVES = ['str', 'dex', 'con', 'int', 'wis', 'cha', 'death'];
Config.EFFECT_FILTER_SCORES = ['ability', 'ac', 'max-hp', 'passive', 'prof', 'speed', 'dc', 'carry'];
Config.NPC_FEATURES = ['none', 'action', 'bonus', 'reaction', 'legendary', 'lair'];
Config.EFFECT_RESOURCE_DICE_POOL = [4, 6, 8, 10, 12, 20];
Config.EFFECT_RESOURCE_RECHARGE_CALC = ['all', 'formula'];
Config.EFFECT_SAVE = ['half', 'none'];
Config.EFFECT_SCALING_METHODS = ['spell', 'cantrip', 'genefunk', 'resource', 'level', 'class'];
Config.EFFECT_SUMMON_BONUSES = ['abl', 'prof', 'chr', 'cls', 'hp', 'spell', 'upcast'];
Config.EFFECT_TARGETS = ['self', 'individual', 'area'];
Config.EFFECT_TARGETS_AREA = ['cone', 'cube', 'cylinder', 'line', 'sphere'];
Config.ENCUMBRANCE_SIZE_MOD = {tiny: 0.5, sm: 1, med: 1, lg: 2, huge: 4, grg: 8};
Config.ENCUMBRANCE_THRESHOLDS = {encumbered: 2.33, heavy: 4.67};
Config.FEAT_ACTION = ['none', 'action', 'bonus', 'reaction', 'special'];
Config.FEAT_TRIGGERS = ['hit', 'start', 'failroll', 'spellcast'];
Config.FEAT_SOURCE_TYPES = ['class', 'race', 'feat', 'upgrade', 'other'];
Config.FEAT_USES_KEYS = ['abl', 'chr', 'cls', 'prof'];
Config.HD = [2, 4, 6, 8, 10, 12, 20];
Config.INVENTORY_ITEMS = new Set(['weapon', 'equipment', 'consumable', 'backpack', 'tool', 'loot']);
Config.ITEM_CHARGE_DICE = [2, 3, 4, 6, 8, 10, 12, 20];
Config.ITEM_RARITY = ['c', 'uc', 'r', 'vr', 'l', 'a'];
Config.ITEM_RECHARGE = ['long', 'short', 'dawn', 'dusk', 'encounter', 'session', 'never', 'roll'];
Config.ITEM_SUBRARITY = ['min', 'maj'];
Config.MAX_LEVEL = 20;
Config.NPC_SIZE_HD = {tiny: 4, sm: 6, med: 8, lg: 10, huge: 12, grg: 20};
Config.PROF_ARMOUR = ['lgt', 'med', 'hvy'];
Config.PROF_WEAPON = [
	'sim', 'marm', 'marr'
];

Config.PROF_LANG = [
	'all','english','mandarin','hindi','spanish','french','arabic','danish',
	'russian','portuguese','indonesian','swedish','german','japanese','swahili',
	'marathi','telugu','italian','korean','greek','turkish','dutch','cantonese','isl'
];

Config.PROF_TOOLS = ['acad','artt','mili','itga','undw','reli','hmra','itsp','tech','hbet'];

Config.ALL_TOOLS =
	Config.PROF_TOOLS;

Config.PLUS_PROF = {
	0.5: 'half',
	1: 'prof',
	2: 'expert'
};

Config.PROFICIENCY_LEVELS = {
	0: 'none',
	0.5: 'half',
	1: 'prof',
	2: 'expert'
};

Config.RESOURCE_USES = ['fixed', 'formula'];
Config.ROLL = ['reg', 'adv', 'dis'];
Config.SKILLS = [
	'acr', 'ani', 'arc', 'ath', 'dec', 'his', 'ins', 'inv', 'itm', 'med',
	'nat', 'per', 'prc', 'prf', 'rel', 'slt', 'ste', 'sur'
];

Config.SPEEDS = ['walk', 'burrow', 'climb', 'fly', 'swim'];
Config.SPELL_COMPONENT_MAP = {v: 'Verbal', s: 'Somatic', m: 'Material', r: 'Royalty'};
Config.SPELL_CAST_TIMES = ['action', 'ba', 'react', 'min', 'hour', 'day', 'week', 'special'];
Config.SPELL_DURATIONS = ['instant', 'dispel', 'special', 'round', 'min', 'hour', 'day'];
Config.SPELL_RANGES = ['self', 'touch', 'short', 'long', 'unlimited'];
Config.SPELL_SCHOOLS = ['inj', 'gad', 'cte', 'ctb', 'cex', 'bot', 'sof', 'min', 'mis', 'mib'];
Config.SPELL_SOURCES = ['class', 'custom'];
Config.SPELL_PREP = ['known'];
Config.SPELL_PROGRESSION = ['third', 'half', 'full', 'pact'];
Config.SPELL_RITUALS = ['none'];

Config.SPELLS_KNOWN_TABLE = {
	biohacker: {
		cantrips: [],
		known: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 14, 15, 16, 17, 17, 18, 19, 20]
	},
	codehacker: {
		cantrips: [],
		known: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 14, 15, 16, 17, 17, 18, 19, 20]
	},
	engineer: {
		cantrips: [],
		known: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 14, 15, 16, 17, 17, 18, 19, 20]
	}
};

Config.VEHICLE_FEATURES = ['none', 'action', 'reaction', 'component', 'siege'];;
Config.VEHICLE_TYPES = ['air', 'legged','submersible','treaded','water','wheeled'];
Config.WEAPON_CATEGORIES = ['simple', 'martial', 'unarmed', 'siege'];
Config.WIND_DIRECTIONS = ['reaching', 'with', 'against'];
