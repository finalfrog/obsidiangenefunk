import {OBSIDIAN} from '../global.js';

export function prepareSpellcasting (actorData, data, flags, derived) {
	const mods = [];
	const attacks = [];
	const saves = [];
	const existing = {};

	derived.spellcasting = {mods, attacks, saves};

	var effectiveSpellcasterLevel = 0;
	for (const cls of derived.classes) {
		const spellcasting = cls.obsidian.spellcasting;
		if (spellcasting?.enabled
			&& !OBSIDIAN.notDefinedOrEmpty(spellcasting.ability)
			&& !existing[spellcasting.ability])
		{
			mods.push(spellcasting.mod);
			attacks.push(spellcasting.attack);
			saves.push(spellcasting.save);
			existing[spellcasting.ability] = true;

			if (!OBSIDIAN.notDefinedOrEmpty(spellcasting.progression))
			{
				if (spellcasting.progression === 'full')
				{
					effectiveSpellcasterLevel += cls.data.data.levels;
				} else if (spellcasting.progression === 'half') {
					effectiveSpellcasterLevel += cls.data.data.levels/2;
				} else if (spellcasting.progression === 'third') {
					effectiveSpellcasterLevel += cls.data.data.levels/3;
				}
			}
		}
	}

	if (actorData.type === 'npc' && !OBSIDIAN.notDefinedOrEmpty(data.attributes.spellcasting)) {
		const mod = data.abilities[data.attributes.spellcasting].mod;
		data.attributes.spellMod = mod;
		data.attributes.spellAtk = mod + data.attributes.prof;
		data.attributes.spelldc = mod + data.attributes.prof + 8;

		if (!OBSIDIAN.notDefinedOrEmpty(flags.spells.attack)) {
			data.attributes.spellAtk = flags.spells.attack;
		}

		if (!OBSIDIAN.notDefinedOrEmpty(flags.spells.save)) {
			data.attributes.spelldc = flags.spells.save;
		}

		if (!existing[data.attributes.spellcasting]) {
			mods.push(data.attributes.spellMod);
			attacks.push(data.attributes.spellAtk);
			saves.push(data.attributes.spelldc);
		}
	}

	// Override DnD 5e slot progression with Genefunk slot progression
	if (effectiveSpellcasterLevel > 0) {
		if (!data.spells.spell1.override) {
			if (effectiveSpellcasterLevel < 5) {
				data.spells.spell1.max = effectiveSpellcasterLevel+3;
			} else if (effectiveSpellcasterLevel < 9) {
				data.spells.spell1.max = 7;
			} else if (effectiveSpellcasterLevel < 13) {
				data.spells.spell1.max = 8;
			} else if (effectiveSpellcasterLevel < 17) {
				data.spells.spell1.max = 9;
			} else {
				data.spells.spell1.max = 10;
			}
		}
		if (!data.spells.spell2.override) {
			if (effectiveSpellcasterLevel < 5) {
				data.spells.spell2.max = 0;
			} else if (effectiveSpellcasterLevel < 10) {
				data.spells.spell2.max = effectiveSpellcasterLevel-4;
			} else {
				data.spells.spell2.max = 5;
			}
		}
		if (!data.spells.spell3.override) {
			if (effectiveSpellcasterLevel < 9) {
				data.spells.spell3.max = 0;
			} else if (effectiveSpellcasterLevel < 14) {
				data.spells.spell3.max = effectiveSpellcasterLevel-8;
			} else {
				data.spells.spell3.max = 5;
			}
		}
		if (!data.spells.spell4.override) {
			if (effectiveSpellcasterLevel < 13) {
				data.spells.spell4.max = 0;
			} else if (effectiveSpellcasterLevel < 18) {
				data.spells.spell4.max = effectiveSpellcasterLevel-12;
			} else {
				data.spells.spell4.max = 5;
			}
		}
		if (!data.spells.spell5.override) {
			if (effectiveSpellcasterLevel < 17) {
				data.spells.spell5.max = 0;
			} else if (effectiveSpellcasterLevel < 20) {
				data.spells.spell5.max = effectiveSpellcasterLevel-16;
			} else {
				data.spells.spell5.max = 4;
			}
		}
		if (!data.spells.spell6.override) {
			data.spells.spell6.max = 0;
		}
		if (!data.spells.spell7.override) {
			data.spells.spell7.max = 0;
		}
		if (!data.spells.spell8.override) {
			data.spells.spell8.max = 0;
		}
		if (!data.spells.spell9.override) {
			data.spells.spell9.max = 0;
		}
	}

	if (data.spells) {
		const levelOverride = flags.spells?.slots.pactLevel;
		if (!OBSIDIAN.notDefinedOrEmpty(levelOverride)) {
			data.spells.pact.level = Number(levelOverride);
		}
	}

	if (data.spells.pact) {
		const levelOverride = flags.spells?.slots.pactLevel;
		if (!OBSIDIAN.notDefinedOrEmpty(levelOverride)) {
			data.spells.pact.level = Number(levelOverride);
		}
	}

	if (actorData.type === 'npc' && !OBSIDIAN.notDefinedOrEmpty(flags.spells?.slots.pactLevel)) {
		const lvl = data.details.spellLevel;
		data.spells.pact = data.spells.pact || {};
		data.spells.pact.level = Number(flags.spells?.slots.pactLevel);

		if (data.spells.pact.override) {
			data.spells.pact.max = data.spells.pact.override;
		} else {
			data.spells.pact.max =
				Math.max(1, Math.min(lvl, 2), Math.min(lvl - 8, 3), Math.min(lvl - 13, 4));
		}

		data.spells.pact.value = Math.min(data.spells.pact.value, data.spells.pact.max);
	}
}
