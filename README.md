# Genefunk 2090 Obsidian Character Sheets
A fork of the Obsidian [Foundry VTT](https://foundryvtt.com/) module for Genefunk 2090.

![character sheet](https://bitbucket.org/finalfrog/obsidiangenefunk/raw/genefunkMain/katya.webp)

![npc sheet](https://bitbucket.org/finalfrog/obsidiangenefunk/raw/genefunkMain/ucwar.webp)

![vehicle layout](https://bitbucket.org/finalfrog/obsidiangenefunk/raw/genefunkMain/fighter.webp)

## Bug Reports
If you discover an issue with the sheet or any other functionality the module provides, please report it in the [issue tracker](https://bitbucket.org/finalfrog/obsidiangenefunk/issues) and ensure you include the following information:

1. The version of foundry, obsidiangenefunk, and dnd5e that you are using.
2. Whether the issue is a bug or a module incompatibility. You can tell the difference by first disabling all other modules apart from obsidiangenefunk. If you still experience the issue then it is a bug, otherwise it is a module incompatibility.
3. Any error messages that appear when the issue manifests, or any error messages that appear when the world first loads. You can see these messages by pressing F12 to bring up the console. Please include screenshots or copy-pastes of these errors if you see them.
4. The steps you took to encounter the issue.
5. What you expected to happen.
6. What actually happened.

## License
All source code (`*.html`, `*.css`, `*.js`, `*.json`) is copyright 2019-2022 Kim Mantas, with the exception of individual contributions which remain the copyright of their respective contributors, as specified in the `CONTRIBUTORS` file.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

A copy of this license is available in the `LICENSE` file, or can alternatively be found here: [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html).

The fonts Roboto and Roboto Condensed found in the `fonts` directory are used and redistributed under the terms of the [Apache License v2](http://www.apache.org/licenses/LICENSE-2.0). A copy of this license is available in the `fonts/LICENSE-2.0.txt` file.

The fonts Noto Sans JP and Noto Sans TC found in the `fonts` directory are used and redistributed under the terms of the [OFL 1.1](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web). A copy of this license is available in the `fonts/OFL.txt` file.

The following SVG images found in the `img` directory are used and redistributed under the terms of [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/), with their attributions appearing below:

* [Delapouite](http://delapouite.com/): `versatile.svg`, `healing.svg`, `boots.svg`, `gauntlet.svg`, `bracers.svg`, `cloak.svg`, `belt.svg`, `ring.svg`, `gear.svg`, `ammo.svg`, `trinket.svg`, `provides-spells.svg`, `roll-modifier.svg`, `filter.svg`, `produce-resource.svg`, `uses-ability.svg`, `description.svg`, `roll-table.svg`, `d20.svg`, `bag.svg`
* [Lorc](http://lorcblog.blogspot.com/): `melee.svg`, `ranged.svg`, `bludgeoning.svg`, `piercing.svg`, `slashing.svg`, `psychic.svg`, `radiant.svg`, `thunder.svg`, `poison.svg`, `acid.svg`, `cold.svg`, `force.svg`, `lightning.svg`, `weapon.svg`, `tool.svg`, `armour.svg`, `helm.svg`, `consumable.svg`, `consumable_alt.svg`, `amulet.svg`, `effect.svg`, `demolish.svg`, `resource.svg`, `damage.svg`, `save.svg`, `scaling.svg`, `target.svg`, `consume-resource.svg`, `scroll.svg`, `bonus.svg`, `duration.svg`, `npc.svg`, `loot.svg`, `expression.svg`, `applied-effect.svg`, `defense.svg`, `set-score.svg`, `check.svg`, `extra-crit.svg`, `vehicle.svg`, `greatsword.svg`, `ap_bludgeoning.svg`, `ap_piercing.svg`, `ap_slashing.svg`, `armor_heavy.svg`, `armor_light.svg`, `armor_medium.svg`, `bullets.svg`, `tool_alt.svg`, `feat.svg`, `mask.svg`, `goggles.svg`, `gauntlet_alt.svg`, `drugs.svg`, `drugs_alt.svg`, `biohacker.svg`, `consumeable_alt2.svg`, `suit.svg`, `gunfighter.svg`, `crook.svg`, `codehacker.svg`, `samurai.svg`, `engineer.svg`, `sack.svg`, `bite.svg`, `letter.svg`, `trap.svg`, `hardcase.svg`, `book.svg`
* [Skoll](https://game-icons.net/): `unarmed.svg`, `necrotic.svg`, `wand.svg`, `food.svg`
* [sbed](https://opengameart.org/content/95-game-icons): `fire.svg`
* [Willdabeast](http://wjbstories.blogspot.com/): `rod.svg`, `lightbludgeoning.svg`
* [Rawdanitsu] (https://opengameart.org/content/36-free-black-and-white-icons): `handgun.svg`, `object.svg`
* <Chainsaw by Vectors Market from the Noun Project> [Vectors Market] (https://thenounproject.com/vectorsmarket) {This work has been modified}: `chainsaw.svg`
* <Machine Gun by Vectors Market from the Noun Project> [Vectors Market] (https://thenounproject.com/vectorsmarket) {This work has been modified}: `minigun.svg`
* <Laser Gun by Vectors Market from the Noun Project> [Vectors Market] (https://thenounproject.com/vectorsmarket) {This work has been modified}: `lasergun.svg`
* <columbia knife by Nattawut Buahom from the Noun Project> [Nattawut Buahom] (https://thenounproject.com/satan_nat555) {This work has been modified}: `throwingblade.svg`
* <Samurai by Dicron Studio from the Noun Project> [Dicron Studio] (https://thenounproject.com/ilhampratomo) {This work has been modified}: `lightblade.svg`
* <katana by Zachary J Savoy from the Noun Project>[Zachary J Savoy] (https://thenounproject.com/ZachJSavoy ) {This work has been modified}: `katana.svg`
* <Machine Gun by Creative Stall from the Noun Project> [Creative Stall] (https://thenounproject.com/creativestall) {This work has been modified}: `machinegun.svg`
* <sniper by Andrejs Kirma from the Noun Project> [Andrejs Kirma] (https://thenounproject.com/andrejs) {This work has been modified}: `sniper.svg`
* <sniper by Hea Poh Lin from the Noun Project> [Hea Poh Lin] (https://thenounproject.com/charlenehea/) {This work has been modified}: `hunting.svg`
* <Shotgun by Kieu Thi Kim Cuong from the Noun Project> [Kieu Thi Kim Cuong] (https://thenounproject.com/kieukimcuong/) {This work has been modified}: `shotgun.svg`
* <Smg by Ian Rahmadi Kurniawan from the Noun Project> [Ian Rahmadi Kurniawan] (https://thenounproject.com/irk.aminin) {This work has been modified}: `smg.svg`
* <Grenade by Mello from the Noun Project> [Mello] (https://thenounproject.com/stonuiiuntk/) {This work has been modified}: `grenade.svg`
* <Bazooka by Mello from the Noun Project> [Mello] (https://thenounproject.com/stonuiiuntk/) {This work has been modified}: `bazooka.svg`
* <Rifle by Mello from the Noun Project> [Mello] (https://thenounproject.com/stonuiiuntk/) {This work has been modified}: `rifle.svg`
* <DNA by I Create Stuff from the Noun Project> [I Create Stuff] (https://thenounproject.com/ICStuff/) {This work has been modified}: `gene.svg`

The SVG images `img/compendium.svg`, and `img/roll-table.svg` are used and redistributed under the terms of [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/), and is attributed to the [Font Awesome Free](https://fontawesome.com/) Project.

The following PNG images in the `img` directory (`obsidian.png`, `damage-reduction.png`, `immunity.svg`, `resistance.svg`, `vulnerability.svg`), and all images in the `img/conditions`, `img/currency`, and `img/magic` directories are  copyright 2019-2022 [mikiko](https://mikiko.art) and are used and redistributed under the terms of [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

The following PNG images in the `img` directory (`Bot.png`, `CraftExplosives.png`, `CraftTech.png`, `CraftTechBot.png`, `Gadget.png`, `Injection.png`, `Mind.png`, `MindBot.png`, `MindSoftware.png`, `Software.png`) are copyright 2021 James Armstrong and are used and redistributed with permission.

The following PNG images in the `img` directory (`satoshi.png`, `bit.png`, `bitcoin.png`) are used and redistributed under the terms of [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).

The following JPG images in the `img` directory (`Canary.jpg`, `Coelhomortos.jpg`, `Companion.jpg`, `Espion.jpg`, `Eucypher.jpg`, `Feral.jpg`, `Leviathan.jpg`, `Mutt.jpg`, `Optimized.jpg`, `OsaLaska.jpg`, `Sherlock.jpg`, `Spartan - Infantry.jpg`, `Spartan - Naiad.jpg`, `Spartan - Wraith.jpg`, `Titan.jpg`, `Traceur.jpg`, `Transhuman.jpg`, `Unmodified.jpg`) are liscenced by James Armstrong and used and redistributed with permission.

The following PNG images in the `img` directory (`Doekkebi1.png`, `Soldier1.png`, `aerial drone 1.png`, `aerial drone 2.png`, `aerial drone 3.png`, `amaranth (purple).png`, `amaranth (yellow).png`, `ape man dealer.png`, `armored police 1.png`, `armored police 2.png`, `armored police 3.png`, `armored soldier 1.png`, `armored soldier 2.png`, `armored solider 3.png`, `asura 1.png`, `asura 2.png`, `asura 3.png`, `bioengineered huge reptile.png`, `biohacker .png`, `biohacker 1.png`, `biohacker 2.png`, `biohacker 3.png`, `biohacker 4.png`, `biohacker 5.png`, `biohacker 6.png`, `biohacker7.png`, `burrow worm.png`, `canary 1.png`, `canary 2.png`, `canary 3.png`, `canary4.png`, `cephalopsych.png`, `codehacker 1.png`, `codehacker 2.png`, `codehacker 3.png`, `codehacker 4.png`, `codehacker 6.png`, `coelhomortos hardcase.png`, `coelhomortos.png`, `combat engineer.png`, `combat medic.png`, `companion 1.png`, `crime boss.png`, `cyborg 1.png`, `cyborg 2.png`, `cyborg 4.png`, `cyborg grinder.png`, `cyborg suit.png`, `cyborg3.png`, `Dec 11 10:40 desktop.ini`, `DefaultToken.png`, `detective 1.png`, `dokkaebi 2.png`, `dokkaebi 3.png`, `droid.png`, `engineer 1.png`, `engineer 2.png`, `eucypher 1.png`, `eucypher 2.png`, `femme fatale.png`, `feral 2.png`, `feral 3.png`, `feral 4.png`, `feral engineer.png`, `goemul.png`, `goliath droid.png`, `gunfighter 2.png`, `gunfighter 3.png`, `gunfighter.png`, `gunslinger 1.png`, `hanneul .png`, `hardcase 1.png`, `hardcase 2.png`, `hardcase 3.png`, `hardcase 4.png`, `hardcase cyborg.png`, `hitman .png`, `jungle.png`, `leviathan 1.png`, `leviathan 2.png`, `leviathan 3.png`, `markus.png`, `mea droid.png`, `mutt 1.png`, `mutt 2.png`, `mutt 3.png`, `mutt 4.png`, `mutt 5.png`, `mutt engineer 2.png`, `mutt engineer.png`, `mutt gearhead.png`, `mutt hardcase.png`, `naiad 1.png`, `naiad 2.png`, `naiad 3.png`, `naiad child.png`, `namukkun 1.png`, `namukkun 2.png`, `oknytt 1.png`, `oknytt 2.png`, `oknytt 3.png`, `oknytt 4.png`, `osalaska 1.png`, `osalaska 2.png`, `quadruped drone 1.png`, `quadruped drone 2.png`, `reaper mantis.png`, `recombinant ape.png`, `recombinant beast canine.png`, `recombinant canine 2.png`, `recombinant raccoon.png`, `samurai 1.png`, `samurai 2.png`, `samurai 3.png`, `samurai4.png`, `sherlock 1.png`, `sherlock 2.png`, `sniper 1.png`, `sniper 2.png`, `spartan infantry 1.png`, `spartan infantry 2.png`, `spartan infantry 3.png`, `spartan infantry 4.png`, `spartan infantry 5.png`, `spartan infantry 6.png`, `suit 1.png`, `suit 2.png`, `suit commander.png`, `tarantula.png`, `thief 1.png`, `thief 2.png`, `titan 1.png`, `titan 2.png`, `traceur.png`, `transuman 1.png`, `unarmed fighter.png`, `vampire thug.png`, `wraith 1.png`, `wraith 2.png`, `wrath tweaker.png`, `yakuza.png`, `Maxim.png`, `Mayor Noh.png`, `Tokenmaster3.png`, `aldous laplace.png`, `bao zhai.png`, `bungle.png`, `cao cao.png`, `choi eun joo.png`, `daichi.png`, `eun sook.png`, `gwoesu.png`, `gyuri kim.png`, `junko minami.png`, `junsu.png`, `min soo.png`, `miroslav.png`, `mr quick.png`, `naga.png`, `olya zietzev.png`, `sasha.png`, `seraph 1.png`, `seraph 2.png`, `song giduk.png`, `soojin.png`, `sophia choi.png`, `tetsuo.png`, `the stoker.png`, `timur.png`, `traktor.png`, `yong chul.png`, `BloodDoll.png`, `Drohneschreck.png`, `Mammoth.png`, `Munki Drone.png`, `Occisorsaurus Rex.png`, `Psymicid.png`, `Recombinant Feline.png`, `Stirge.png`) are liscenced by James Armstrong and used and redistributed with permission.

The following PNG images in the `img` directory (`afv.png`, `airbus.png`, `attack_helicopter.png`, `fighter_jet.png`, `military_airbus.png`, `transport_helicopter.png`) are copyright [Grantovich](https://www.patreon.com/Grantovich) and used and redistributed with permission.

The following PNG images in the `img` directory (`light_tank.png`, `mini_sub.png`, `moped.png`, `motorbike_basic.png`, `motorbike_premium.png`, `personal_helicopter.png`, `sedan_basic.png`, `sedan_luxury.png`, `small_private_jet.png`, `spider_tank.png`, `sports_car.png`, `truck.png`) are copyright [Gabriel Pickard](https://marketplace.roll20.net/browse/publisher/64/gabriel-pickard) and used and redistributed with permission.
